global main
extern printf

section .data
	wrongnumberparam: db 'Wrong number of parameters supplied. Expected 1 parameter.', 10, 0
	wrongparamlength: db 'Wrong length for parameter. Expected 9 characters.', 10, 0
	format: db '%s-%1d', 10, 0
	checksum_error_message: db 'Unknown character: %c', 10, 0
	
section .text
main:
;Check for param
	mov eax, [esp+4]	;argc
	cmp eax, 2		;Compare number of parameters to 2 (Filename + acutal param)
	jne param_error		;Jump to error output if eax != 2
;Read param	
	mov eax, [esp+8]	;argv
	mov ebx, [eax+4]	;start of first argument
;Check param length
	push ebx		;Push parameter
	call strlen		;call strlen
	add esp, 4		;Fix stack
	cmp eax, 9		;Compare result to 9 (Expected strlen)
	jne param_length_error	;Error if not equal
;Call checksum
	push ebx		;push ebx as param for checksum
	call checksum		;call checksum
	add esp, 4		;fix stack
	cmp eax, 10
	je finish
;Output
	push DWORD eax		;push eax to stack
	push DWORD ebx		;push ebx to stack
	push format		;push output format to stack
	call printf		;call printf
	add esp, 12		;fix stack
finish:
	ret			;return

param_error:
	push wrongnumberparam	;push error text
	call printf		;call printf
	add esp, 4		;fix stack
	ret			;return
param_length_error:
	push wrongparamlength	;Push error text
	call printf		;print
	add esp, 4		;Fix stack
	ret			;return

strlen:
	push edi		;Save edi
	mov edi, [esp+8]	;Get param from stack
	sub ecx, ecx		;Init ecx as 0
	sub al, al		;Init al as 0
	not ecx			;Reverse ecx bits to get ecx to highest value possible
	cld			;Clear direction flag
	repne scasb		;Repeat if not equal scasb
	not ecx			;Reverse ecx to get number of characters + 1
	dec ecx			;Remove +1
	mov eax, ecx		;Move ecx to eax, to set as return value
	pop edi			;Restore edi
	ret			;Return

checksum:			;Checksum calculation
checksum_init:			;Init checksum calculation
	push ebx		;Save ebx
	mov ebx, [esp+8]	;Set ebx to first address
	mov ecx, 0		;Init counter
	mov eax, 0		;Init sum
checksum_read:
	mov edx, 0		;Overwrite preceding calculation
	mov dl, byte [ebx]	;Get value of address
	cmp dl, 0		;Compare to NUL
	je checksum_remainder	;End of characters
	cmp dl, '0'		;Compare to '0' => 48
	jl checksum_undefined	;If less abort
	cmp dl, '9'		;Compare to '9' => 57
	jle checksum_int	;If less or equal -> integer to read
	cmp dl, 'A'		;Compare to 'A' => 65
	jl checksum_undefined	;If less abort
	cmp dl, 'Z'		;Compare to 'Z' => 90
	jle checksum_letter_uppercase	;If less or equal -> uppercase character to read
	cmp dl, 'a'		;Compare to 'a' => 97
	jl checksum_undefined	;If less abort
	cmp dl, 'z'		;Compare to 'z' => 122
	jle checksum_letter_lowercase	;If less or equal -> lowercase character to read
	jmp checksum_undefined	;If out of range abort
checksum_int:
	sub dl, '0'		;Subtract '0' => 48 from dl
	jmp checksum_check	;Go to checksum_check
checksum_letter_lowercase:
	sub dl, 'a'		;Subtract 'a' => 97 from dl
	add dl, 10		;Add 10 to dl
	jmp checksum_check	;Go to checksum_check
checksum_letter_uppercase:
	sub dl, 'A'		;Subtract 'A' => 65 from dl
	add dl, 10		;Add 10 to dl
	jmp checksum_check	;Go to checksum check
checksum_undefined:
	jmp checksum_error	;Print error and abort execution
checksum_check:
	cmp ecx, 0		;Compare counter to 3
	je checksum_seven	;First group of 3
	cmp ecx, 1		;Compare counter to 6
	je checksum_three	;Second group of 3
	cmp ecx, 2		;Compare counter to 9
	je checksum_add		;Third group of 3
	jmp checksum_remainder	;Go to finish
checksum_seven:
	imul edx, 7		;Multiply by 7
	jmp checksum_add	;Go to checksum_next
checksum_three:
	imul edx, 3		;Multiply by 3
	jmp checksum_add	;Go to checksum_next
checksum_add:
	add eax, edx		;Add to sum
checksum_next:
	inc ebx			;Move pointer to next address
	cmp ecx, 2		;Reset if 2
	jge checksum_counter_reset	;Reset counter
checksum_counter_increment:
	inc ecx			;Increment counter
	jmp checksum_read	;Repeat
checksum_counter_reset:
	mov ecx, 0		;Reset counter
	jmp checksum_read	;Repeat
checksum_remainder:
	mov ebx, 10		;Set ebx to 10
	div ebx			;Divide eax by 10
	mov eax, edx		;Get remainder and store into eax
checksum_finish:
	pop ebx			;Reset ebx
	ret			;Finish
checksum_error:
	push edx		;Push edx
	push checksum_error_message	;Push message
	call printf		;Print
	add esp, 8		;Fix stack
	mov eax, 10
	jmp checksum_finish	;Go to finish
